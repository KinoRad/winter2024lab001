import java.util.Random;
public class Calculator{
	public static int sum(int num1, int num2){
		return num1 + num2;
	}
	public static double root(int num){
		return Math.sqrt(num);
	}
	public static int random(){
		Random rando = new Random();
		return rando.nextInt(100);
	}
	public static double divid(int num1, int num2){
		if(num1 == 0 || num2 == 0){
			return 0;
		}
		return num1/num2;
	}
}