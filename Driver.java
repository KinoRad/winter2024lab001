import java.util.Scanner;
public class Driver{
	public static void main(String [] args){
		Scanner reader = new Scanner(System.in);
		System.out.println("Hello user!");
		System.out.println("enter 2 numbers to add together!");
		int num1 = reader.nextInt();
		int num2 = reader.nextInt();
		System.out.println(Calculator.sum(num1, num2));
		System.out.println("enter a number to squareroot!");
		int rootNum = reader.nextInt();
		System.out.println(Calculator.root(rootNum));
		System.out.println("Here is a random number: ");
		System.out.print(Calculator.random());
	}
}