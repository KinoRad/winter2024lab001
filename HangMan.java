import java.util.Scanner;

/*
- Prompts the user to input a 4-letter word with no repeating characters.
- Calls the runGame() method with that word.
*/
public class HangMan {
	/*
	- Takes as input (String word) a 4-letter string with no repeating char.
	- Also takes as input (char letter) a character. 
	- Uses a loop to compare every char in that string with the letter. 
		If there is a match, it returns its index. 
		Else, there it will return -1.
	*/
	public static int isLetterInWord(String word, char letter) {
		int count = 0;
		while (count <= 3) {
			if (toUpperCase(word.charAt(count)) == toUpperCase(letter)) {
				return count;
			}
			else {
				count++;
			}
		}
		return -1;
	}
	
	/*
	- Takes a char (c) as input.
	- Returns the uppercase of that char regardless of whether that char is already uppercase or not.
	*/
	public static char toUpperCase(char c) {
		if (c >= 'a' && c <= 'z') { 
			return c -= 32;
		}
		else {
			return c;
		}
	}
	
	/*
	- Takes (String word) as input; it must be 4 letters.
	- Also takes 4 booleans as input (letter0, letter1, letter2, letter3), representing whether
		the letter has been guessed or not [true = guessed, false = not guessed].
	- Checks each char in a string to see if it's true. If it is, it will display that char, 
		otherwise it's place will be represented by a "_".
	- Outputs a string showing the guessed letters and the non-guessed ones.
	- Example : with parameters ("bleh", true, false, false, true), the output will be: "Your result is b__h".
	*/
	public static void printWork(String word, boolean letter0, boolean letter1, boolean letter2, boolean letter3) {
		String output = "Your result is ";
		if (letter0 == true) {
			output += word.charAt(0);
		}
		else {
			output += "_";
		}
		
		if (letter1 == true) {
			output += word.charAt(1);
		}
		else {
			output += "_";
		}
		
		if (letter2 == true) {
			output += word.charAt(2);
		}
		else {
			output += "_";
		}
		
		if (letter3 == true) {
			output += word.charAt(3);
		}
		else {
			output += "_";
		}
		
		System.out.println(output);
	}
	
	/*
	- Takes as input a 4-letter string with no repeating characters.
	- Asks the user to input a letter [guess].
	- If the letter is correctly guessed from the word,
		use the printWork() method to display the position of the letter.
	- Else it will increase the number of misses.
	- The loop stops if all letters are correctly guessed or if the user guesses wrong 6 times.
	- Displays a message telling the user if they won the game or not.
	*/
	public static void runGame(String word) {
		java.util.Scanner reader = new java.util.Scanner(System.in);
		
		int countOfMisses = 0;
		char inputChar;
		int inputCharIndex;
		
		boolean letter0 = false;
		boolean letter1 = false;
		boolean letter2 = false;
		boolean letter3 = false;
		boolean areAllLettersTrue = (letter0 == true && letter1 == true && letter2 == true && letter3 == true);
		
		while (!areAllLettersTrue && countOfMisses < 6) {
			System.out.println("enter a letter:");
			inputChar = reader.next().charAt(0);
			inputCharIndex = isLetterInWord(word, inputChar);
			
			if (inputCharIndex == 0) {
				letter0 = true;
				printWork(word, letter0, letter1, letter2, letter3);
			}
			else if (inputCharIndex == 1) {
				letter1 = true;
				printWork(word, letter0, letter1, letter2, letter3);
			}
			else if (inputCharIndex == 2) {
				letter2 = true;
				printWork(word, letter0, letter1, letter2, letter3);
			}
			else if (inputCharIndex == 3) {
				letter3 = true;
				printWork(word, letter0, letter1, letter2, letter3);
			}
			else {
				countOfMisses++;
				printWork(word, letter0, letter1, letter2, letter3);
			}
			areAllLettersTrue = (letter0 == true && letter1 == true && letter2 == true && letter3 == true);
		}
		if (areAllLettersTrue == true) {
			System.out.println("You win!");
		}
		else {
			System.out.println("Better luck next time.");
		}
	}
}