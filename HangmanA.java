import java.util.Scanner;

/* Game of Hangman
* Word 4 letters
* Asks for four letter word input from user
* Asks for letter guesses from user
* Displays letters guessed
* 6 wrong guesses allowed
* Does not allow for double letter words
*/
public class HangmanA {
	
	/* Asks user for a four letter word
	* Runs runGame
	*/
	public static void main (String args []) {
		Scanner reader = new Scanner(System.in);
		System.out.println("Enter a 4 letter word");
		String word = reader.next();
		runGame(word);
	}
	
	/* Checks to see is letter is in the word
	* Takes string word and char c as input
	* Uses for loop and methods toUpperCase to compair
		c to the characters in word
	* Return the index of the char in the word
	* Returns -1 if it is not in the word
	*/
	public static int isLetterlnWord(String word, char c) {
		for(int i = 0; i < word.length(); i++){
			if (toUpperCase(word.charAt(i)) == toUpperCase(c)) {
				return i;
			}
		}return -1;
	}
	/*Takes char c as input
	* Checks to see if letter is lower case
	* If true it subtracts 32 to change it to upper case
	* If false it returns the letter as is
	*/
	public static char toUpperCase(char c) {
		if (c >= 'a' & c <= 'z') {
			return (c -= 32);
		}
		return c;
	}
	
	/* Takes string word, and a array of booleans as input
	*Output "Your result is"
	* Then outputs a "_" for letter that have not been guessed
	* Or output the letter if guessed
	* Uses a for loop to do this
	*/
	public static void printWork(String word, boolean [] wordArray){
		System.out.print("Your result is ");
		for (int i = 0; i < word.length(); i++){
			if(wordArray[i]){
				System.out.print(word.charAt(i));
			}else {
				System.out.print("_");
			}
		}
		System.out.println();
	}
	
	/*Takes string word as input
	* initialize an array of booleans that are all false
	* initialize wrongGuesses to 0
	* initialize correctGuesses to 0
	* While loop = Checks if correctGuesses is less than 4 and 
		wrongGuesses is less than 6
	* Then asks user for a letter
	* check if user guess is correct or not, adjust the variables 
		accordingly and calls printWork
	* It repets for the other letter
	* Then if checks if correctGuesses is = 4, if true it prints "You win!"
	* If false, it prints "Better luck next time."
	*/
	public static void runGame(String word) {
		Scanner reader = new Scanner(System.in);
		boolean [] letters = new boolean[]{false, false, false, false};
		int wrongGuesses = 0;
		int correctGuesses = 0;
		while (correctGuesses < 4 && wrongGuesses < 6) {
			System.out.println("Enter a letter");
			char c = reader.next().charAt(0);
			int positionOfChar = isLetterlnWord(word, c);
			if(positionOfChar != -1 && !(letters[positionOfChar])){
				letters[positionOfChar] = true;
				correctGuesses++;
			}else{
				wrongGuesses++;
			}
			printWork(word, letters);
		}
		if (correctGuesses == 4) {
			System.out.println("You win!");
		} else {
			System.out.println("Better luck next time.");
		}
	}
}